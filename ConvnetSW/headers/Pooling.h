#ifndef POOLING_H
#define POOLING_H

class Pooling
{
	unsigned int poolingDimension;
	unsigned int inputWidth;
	unsigned int inputHeight;
	unsigned int* inputMemory;
	unsigned int* outputMemory;

public:
	Pooling(unsigned int poolingDimension, unsigned int inputWidth, unsigned int inputHeight,
			unsigned int* inputMemory, unsigned int* outputMemory);

	void Pool();
};

#endif
