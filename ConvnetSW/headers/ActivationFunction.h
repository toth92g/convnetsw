#ifndef ACTIVATIONFUNCTION_H
#define ACTIVATIONFUNCTION_H

class ActivationFunction
{
	unsigned int dataWidth;
	unsigned int intervalLimit = 6;

public:
	ActivationFunction(unsigned int dataWidth);

private:
	double Sigmoid(double input);

public:
	void generateActivationFunction(char* path);

};

#endif
