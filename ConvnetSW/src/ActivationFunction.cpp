#include <math.h>
#include <fstream>
#include "ActivationFunction.h"

/* Constructor */
ActivationFunction::ActivationFunction(unsigned int dataWidth)
{
	this->dataWidth = dataWidth;
}

/* Calculates the output of the sigmoid function:  y = 1 / (1 + e^(-x)) */
double ActivationFunction::Sigmoid(double input)
{
	return (1 / (1 + exp(-input)));
}

/* Generates a verilog memory initialization text file */
void ActivationFunction::generateActivationFunction(char* path)
{
	double looper;
	double Id = 0;
	double resolution = 2*intervalLimit/pow(2, dataWidth);

	double inputValue;				// Input value of the sigmoid function
	double outputValue;				// Output value of the sigmoid function
	unsigned int outputIntValue;	// Integer output value of the sigmoid function

	std::ofstream memoryFile;
	memoryFile.open(path);

	for(looper = 0; looper < 2*intervalLimit; looper += resolution)
	{
		inputValue = looper - intervalLimit;
		outputValue = Sigmoid(inputValue);
		outputIntValue = (unsigned int)(outputValue * pow(2, dataWidth));

		memoryFile <<  "Memory[" << Id << "] = " << outputIntValue << ";\t" << "/* In: " << inputValue << "\t Out: "<< outputValue <<" */" << std::endl;

		Id++;
	}

	memoryFile.close();
}


