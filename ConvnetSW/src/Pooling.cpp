#include "Pooling.h"


Pooling::Pooling(unsigned int poolingDimension, unsigned int inputWidth, unsigned int inputHeight,
		unsigned int* inputMemory, unsigned int* outputMemory)
{
	this->poolingDimension = poolingDimension;
	this->inputWidth = inputWidth;
	this->inputHeight = inputHeight;
	this->inputMemory = inputMemory;
	this->outputMemory = outputMemory;
}

void Pooling::Pool()
{
	unsigned int baseX;
	unsigned int baseY;
	unsigned int poolingX;
	unsigned int poolingY;
	unsigned int baseAddress;
	unsigned int offsetAddress;
	double average;

	for(baseX = 0; baseX < this->inputWidth; baseX += this->poolingDimension)
	{
		for(baseY = 0; baseX < this->inputHeight; baseY += this->poolingDimension)
		{
			baseAddress = baseY * this->inputWidth + baseX;
			average = 0;
			for(poolingY = 0; poolingY < this->poolingDimension; poolingY++)
			{
				for(poolingX = 0; poolingX < this->poolingDimension; poolingX++)
				{
					offsetAddress = poolingY * this->inputWidth + poolingX;
					average += *(this->inputMemory + baseAddress + offsetAddress);
				}
				average /= this->poolingDimension * this->poolingDimension;
			}
		}
	}
}
